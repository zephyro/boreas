<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <section class="main">
    
                <aside class="main__sidebar">

	                <!-- Sidebar -->
                    <?php include('inc/sidebar.inc.php') ?>
	                <!-- -->

                </aside>
    
                <div class="main__content">
                    <div class="main__heading">
                        <h1>Группа 2</h1>
                    </div>

	                <div class="filter_bar">
		                <a href="#" class="filter_bar__elem">
			                <span>Функциональная группа 2</span>
			                <i></i>
		                </a>
		                <div class="filter_bar__result">Найдено 3 устройства</div>
	                </div>



	                <div class="server_group">

		                <div class="server server_red server_elem">

			                <div class="server__status server__status_red">1</div>

			                <div class="server__item">

				                <div class="server__divider">
					                <img src="img/server__top.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit server__unit_alert active">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_05.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit server__unit_alert active">
					                <img src="img/server__block_04.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit server__unit_alert active">
					                <img src="img/server__block_03.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__bottom.jpg" class="img-fluid" alt="">
				                </div>
			                </div>

			                <div class="server__name">1 (DemoRoom)</div>

		                </div>

		                <div class="server server_elem">

			                <div class="server__status"></div>

			                <div class="server__item">

				                <div class="server__divider">
					                <img src="img/server__top.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit active">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_05.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_04.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_03.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__bottom.jpg" class="img-fluid" alt="">
				                </div>
			                </div>

			                <div class="server__name">3 (DemoRoom)</div>

		                </div>

		                <div class="server server_elem">

			                <div class="server__status"></div>

			                <div class="server__item">

				                <div class="server__divider">
					                <img src="img/server__top.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit active">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_05.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_04.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_03.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__divider">
					                <img src="img/server__bottom.jpg" class="img-fluid" alt="">
				                </div>
			                </div>

			                <div class="server__name">3 (DemoRoom)</div>

		                </div>

	                </div>
                    
                </div>
                
            </section>
            
        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
