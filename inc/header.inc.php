<header class="header">
    <div class="header__container">
        <a class="header__logo" href="/">
            <img src="img/logo.png" class="img-fluid" alt="">
        </a>
        <div class="header__wrap">

            <nav class="header__nav">
                <ul>
                    <li><a href="#">Отчеты</a></li>
                    <li><a href="#">Настройки</a></li>
                    <li>
                        <a href="#">
                            <span>Admin</span>
                            <i>
                                <img src="img/icon__exit.svg" class="img-fluid" alt="">
                            </i>
                        </a>
                    </li>
                </ul>
            </nav>

            <div class="header__bottom">

                <div class="header__search" id="header__search">
                                <span class="header__search_toggle" id="header__search_toggle">
                                    <img src="img/icon__search.svg" class="img-fluid" alt="">
                                </span>
                    <div class="header__search_form">
                        <form class="form">
                            <input type="text" class="header__search_input" value="" placeholder="Поиск">
                        </form>
                    </div>
                </div>

                <div class="header__contact">
                    <i>
                        <img src="img/icon__support.svg" class="img-fluid" alt="">
                    </i>
                    <a href="mailto:i.t.support@mail.ru ;">i.t. support@mail.ru ;</a>; <a href="tel:+74959692222">+7 (495) 969-22-22</a>
                </div>

            </div>

        </div>
    </div>
</header>