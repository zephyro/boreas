<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/tippy.js@3/dist/tippy.all.min.js"></script>

<script src="js/vendor/svg4everybody.legacy.min.js"></script>
<script src="js/vendor/smooth-scrollbar/smooth-scrollbar.js"></script>

<script src="js/main.js"></script>