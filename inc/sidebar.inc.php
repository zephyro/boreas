<div class="main__sidebar_toggle"></div>
<div class="main__sidebar_wrap" id="sidebar">
    <nav class="sidenav">
        <ul class="sidenav__primary">
            <li class="open">
                <a href="#">Устройства [12]</a>

                <ul class="sidenav__second">
                    <li class="open">
                        <a href="#">Сервер [5]</a>

                        <ul class="sidenav__third">
                            <li>
                                <a href="#">Сервер Центрального офиса [5]</a>
                            </li>
                            <li>
                                <a href="#">Сервер Орел [2]</a>
                            </li>
                            <li>
                                <a href="#">Сервер СУБД [8]</a>
                            </li>
                        </ul>

                    </li>
                    <li>
                        <a href="#">Коммутатор [4]</a>
                        <ul class="sidenav__third">
                            <li>
                                <a href="#">Коммутатор Центрального офиса [5]</a>
                            </li>
                            <li>
                                <a href="#">Коммутатор Орел [2]</a>
                            </li>
                            <li>
                                <a href="#">Коммутатор СУБД [8]</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">СХД [5]</a>
                        <ul class="sidenav__third">
                            <li>
                                <a href="#">СХД Центрального офиса [5]</a>
                            </li>
                            <li>
                                <a href="#">СХД Орел [2]</a>
                            </li>
                            <li>
                                <a href="#">СХД СУБД [8]</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">JBOD [0]</a>
                    </li>
                </ul>

            </li>
            <li>
                <a href="#">Функциональные группы [21]</a>
                <ul class="sidenav__third">
                    <li>
                        <a href="#">
	                        <span>Группа 1 [8]</span>
	                        <div class="sidenav__status">
		                        <i class="sidenav__status_elem sidenav__status_elem__alert">1</i>
		                        <i class="sidenav__status_elem sidenav__status_elem__warning">2</i>
	                        </div>
                        </a>
                    </li>
                    <li>
	                    <a href="#">
		                    <span>Группа 2 [4]</span>
		                    <div class="sidenav__status">
			                    <i class="sidenav__status_elem sidenav__status_elem__alert">3</i>
			                    <i class="sidenav__status_elem sidenav__status_elem__warning">1</i>
		                    </div>
	                    </a>
                    </li>
                </ul>
            </li>
            <li class="open">
                <a href="#">Модельный ряд [15]</a>
                <ul class="sidenav__second">
                    <li class="open">
                        <a href="#">Сервер [5]</a>
                        <ul class="sidenav__third">
                            <li>
                                <a href="#">
                                    <span>Модель 1 [8]</span>
                                    <div class="sidenav__third_image">
                                        <img src="images/server_01.jpg" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Модель 2 [5]</span>
                                    <div class="sidenav__third_image">
                                        <div class="sidenav__third_image_none">Изображение не найдено</div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Модель 3 [4]</span>
                                    <div class="sidenav__third_image">
                                        <img src="images/server_01.jpg" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Коммутатор [4]</a>
                        <ul class="sidenav__third">
                            <li>
                                <a href="#">
                                    <span>Модель 1 [8]</span>
                                    <div class="sidenav__third_image">
                                        <img src="images/server_01.jpg" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Модель 2 [5]</span>
                                    <div class="sidenav__third_image">
                                        <div class="sidenav__third_image_none">Изображение не найдено</div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Модель 3 [4]</span>
                                    <div class="sidenav__third_image">
                                        <img src="images/server_01.jpg" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">СХД [5]</a>
                        <ul class="sidenav__third">
                            <li>
                                <a href="#">
                                    <span>Модель 1 [8]</span>
                                    <div class="sidenav__third_image">
                                        <img src="images/server_01.jpg" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Модель 2 [5]</span>
                                    <div class="sidenav__third_image">
                                        <div class="sidenav__third_image_none">Изображение не найдено</div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Модель 3 [4]</span>
                                    <div class="sidenav__third_image">
                                        <img src="images/server_01.jpg" class="img-fluid" alt="">
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">JBOD [0]</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>