<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <section class="main">
    
                <aside class="main__sidebar">

	                <!-- Sidebar -->
                    <?php include('inc/sidebar.inc.php') ?>
	                <!-- -->

                </aside>
    
                <div class="main__content">
                    <div class="main__heading">
                        <h1>Демоцентр Boreas Lab</h1>
                    </div>
                    
                    <div class="main__filter">
                        <ul class="main__filter_left">
                            <li>
                                <label>
                                    <input type="radio" name="filter_sort" value="1">
                                    <span>По имени</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="filter_sort" value="2">
                                    <span>По порядку</span>
                                </label>
                            </li>
                        </ul>
                        <ul class="main__filter_right">
                            <li>
                                <label>
                                    <input type="radio" name="filter_show" value="1" checked>
                                    <span>Critical</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="filter_show" value="2">
                                    <span>Warning</span>
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="filter_show" value="3">
                                    <span>All</span>
                                </label>
                            </li>
                        </ul>

                    </div>
                    
                    <div class="server_group">
                        
                        <div class="server server_red">
                            
                            <div class="server__status server__status_red">1</div>
                            
                            <div class="server__item">
    
                                <div class="server__divider">
                                    <img src="img/server__top.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
                                
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover server__unit_alert">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
    
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
                                
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
                                
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
                                
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>
                                
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
                                </div>
                                
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_05.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover server__unit_alert">
                                    <img src="img/server__block_04.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover server__unit_alert">
                                    <img src="img/server__block_03.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__bottom.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                            
                            <div class="server__name">1 (DemoRoom)</div>
                            
                        </div>

                        <div class="server server_yellow">

                            <div class="server__status server__status_yellow">1</div>

                            <div class="server__item">

                                <div class="server__divider">
                                    <img src="img/server__top.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover server__unit_warning">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_05.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_04.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_03.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__unit server__unit_hover">
                                    <img src="img/server__block_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="server__divider">
                                    <img src="img/server__bottom.jpg" class="img-fluid" alt="">
                                </div>
                            </div>

                            <div class="server__name">2 (DemoRoom)</div>

                        </div>

                        <div class="server">

                            <div class="server__status"></div>

	                        <div class="server__item">

		                        <div class="server__divider">
			                        <img src="img/server__top.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_06.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
		                        </div>

		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_05.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_04.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_02.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_03.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_02.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__unit server__unit_hover">
			                        <img src="img/server__block_01.jpg" class="img-fluid" alt="">
		                        </div>
		                        <div class="server__divider">
			                        <img src="img/server__bottom.jpg" class="img-fluid" alt="">
		                        </div>
	                        </div>

                            <div class="server__name">3 (DemoRoom)</div>

                        </div>

	                    <div class="server">

		                    <div class="server__status"></div>

		                    <div class="server__item">

			                    <div class="server__divider">
				                    <img src="img/server__top.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_05.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_04.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_03.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__bottom.jpg" class="img-fluid" alt="">
			                    </div>
		                    </div>

		                    <div class="server__name">3 (DemoRoom)</div>

	                    </div>

	                    <div class="server server_yellow">

		                    <div class="server__status server__status_yellow">1</div>

		                    <div class="server__item">

			                    <div class="server__divider">
				                    <img src="img/server__top.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover server__unit_warning">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_05.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_04.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_03.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__bottom.jpg" class="img-fluid" alt="">
			                    </div>
		                    </div>

		                    <div class="server__name">2 (DemoRoom)</div>

	                    </div>

	                    <div class="server">

		                    <div class="server__status"></div>

		                    <div class="server__item">

			                    <div class="server__divider">
				                    <img src="img/server__top.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_05.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_04.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_03.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__bottom.jpg" class="img-fluid" alt="">
			                    </div>
		                    </div>

		                    <div class="server__name">3 (DemoRoom)</div>

	                    </div>

	                    <div class="server">

		                    <div class="server__status"></div>

		                    <div class="server__item">

			                    <div class="server__divider">
				                    <img src="img/server__top.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_06.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
			                    </div>

			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_05.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_04.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_03.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_02.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__unit server__unit_hover">
				                    <img src="img/server__block_01.jpg" class="img-fluid" alt="">
			                    </div>
			                    <div class="server__divider">
				                    <img src="img/server__bottom.jpg" class="img-fluid" alt="">
			                    </div>
		                    </div>

		                    <div class="server__name">3 (DemoRoom)</div>

	                    </div>

                        <a href="#" class="server server_new">

                            <div class="server__status"></div>

                            <div class="server__item">
                                <img src="img/server.jpg" class="img-fluid" alt="">
                            </div>

                            <div class="server__name"></div>

                        </a>

                    </div>
                    
                </div>
                
            </section>
            
        </div>

        <!-- Modal -->
        <div class="hardware_info" id="myTemplate">
	        My HTML <strong style="color: pink;">tooltip</strong> content
        </div>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
