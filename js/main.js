// SVG IE11 support
svg4everybody();



// Show search
var searchToggle = document.getElementById('header__search_toggle');
searchToggle.onclick = function() {
    console.log('click');
    var el = document.getElementById('header__search');
    el.classList.toggle('open');
    return false;
};



// Sidebar

var toggleNav = document.getElementsByClassName('main__sidebar_toggle')[0];
toggleNav.onclick = function() {
    console.log('click');
    var el = document.getElementsByClassName('main')[0];
    el.classList.remove('open_control');
    el.classList.toggle('open_sidebar');
    return false;
};


var Scrollbar = window.Scrollbar;
Scrollbar.init(document.querySelector('#sidebar'));





var unit =  '<div class="server_info">'+
            '<ul class="server_info__link">'+
                '<li><a href="#"><img src="img/icon__light.svg" class="img-fluid"></a></li>'+
                '<li><a href="#"><img src="img/icon__off.svg" class="img-fluid"></a></li>'+
            '</ul>'+
            '<div class="server_info__title">Сервер DEPO Storm 3400Z2</div>'+
            '<ul class="server_info__list">'+
                '<li><strong>Стойка: </strong>103 (DemoRoom), Позиция: 32</li>'+
                '<li><strong>Юнитов U: </strong>1</li>'+
                '<li><strong>IP: </strong>10.211.2.19</li>'+
                '<li><strong>Серийный номер: </strong>390656-001</li>'+
                '<li><strong>Версия BMC: </strong>3.20</li>'+
                '<li><strong>Версия BIOS: </strong>Depov026</li>'+
                '<li><strong>Протокол подключения: </strong>IPMI</li>'+
            '</div>';


tippy('.server__unit_hover', {
    theme: 'light',
    delay: '150',
    interactive: true,
    interactiveBorder: true,
    placement: 'bottom',
    content: unit
});


(function() {

    $('.device__heading').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.device').toggleClass('open');

    });

}());


(function() {

    $('.sidenav__primary > li > a ').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('li').toggleClass('open');

    });

    $('.sidenav__second > li > a ').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('li').toggleClass('open');

    });

}());

(function() {

    $('.control__indicators_title').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.control__indicators').toggleClass('open');

    });

}());


// Tabs

(function() {

    $('.control__tabs_nav > li > a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.control__tabs');

        $(this).closest('.control__tabs_nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('> .control__tabs_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());