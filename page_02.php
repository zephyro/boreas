<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <section class="main">
    
                <aside class="main__sidebar">

	                <!-- Sidebar -->
                    <?php include('inc/sidebar.inc.php') ?>
	                <!-- -->

                </aside>
    
                <div class="main__content">

	                <ul class="breadcrumb">
		                <li><a href="#">Главная</a></li>
		                <li><a href="#">103 (DemoRoom)</a></li>
		                <li>Стойка</li>
	                </ul>

	                <div class="main__heading">
		                <h1>103 (DemoRoom)</h1>
	                </div>

	                <div class="data">
		                <div class="data__server">

			                <div class="room">
				                <div class="room__header">
					                <div class="server__status server__status_red server__status_inline">1</div>
					                <div class="server__status server__status_yellow server__status_inline">1</div>
					                <a class="room__settings" href="#">
						                <img src="img/icon__setting.svg" class="img-fluid" alt="">
					                </a>
				                </div>
				                <div class="room__row">

					                <div class="room__elem">
						                <div class="server room__server">

							                <div class="server__elem">
								                <img src="img/double_server/ds_top.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_01.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_02.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_03.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_04.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_05.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_bottom.jpg" class="img-fluid" alt="">
							                </div>
						                </div>
					                </div>

					                <div class="room__elem">
						                <div class="server room__server">

							                <div class="server__elem">
								                <img src="img/double_server/ds_top.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_06.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_07.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_10.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_08.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__unit server__unit_hover">
								                <img src="img/double_server/ds_elem_09.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="server__elem">
								                <img src="img/double_server/ds_empty.jpg" class="img-fluid" alt="">
							                </div>

							                <div class="server__elem">
								                <img src="img/double_server/ds_bottom.jpg" class="img-fluid" alt="">
							                </div>
						                </div>
					                </div>

				                </div>
			                </div>

		                </div>
		                <div class="data__info">

			                <div class="data__sensor">
				                <div class="data__sensor_elem sensor_empty">
					                <div class="sensor sensor_gray sensor_center">
						                <div class="sensor__chart">
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__scale" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="256.252" stroke-dashoffset="45.340"></circle>
							                </svg>
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__data" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="191.252" stroke-dashoffset="110.340"></circle>
							                </svg>
							                <div class="sensor__arrow" style="transform: rotate(0deg)"></div>
						                </div>
						                <div class="sensor__value"> </div>
					                </div>
					                <div class="data__sensor_legend">Датчик отсутствует </div>
				                </div>
				                <div class="data__sensor_elem">
					                <div class="sensor sensor_blue sensor_center">
						                <div class="sensor__chart">
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__scale" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="256.252" stroke-dashoffset="45.340"></circle>
							                </svg>
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__data" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="191.252" stroke-dashoffset="110.340"></circle>
							                </svg>
							                <div class="sensor__arrow" style="transform: rotate(94deg)"></div>
						                </div>
						                <div class="sensor__value">40&deg;C</div>
					                </div>
					                <div class="data__sensor_legend">влажность</div>
				                </div>
				                <div class="data__sensor_elem">
					                <div class="sensor sensor_yellow sensor_center">
						                <div class="sensor__chart">
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__scale" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="256.252" stroke-dashoffset="45.340"></circle>
							                </svg>
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__data" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="221.252" stroke-dashoffset="80.340"></circle>
							                </svg>
							                <div class="sensor__arrow" style="transform: rotate(180deg)"></div>
						                </div>
						                <div class="sensor__value">40&deg;C</div>
					                </div>
					                <div class="data__sensor_legend">вибрация</div>
				                </div>
				                <div class="data__sensor_elem">
					                <div class="sensor sensor_red sensor_center">
						                <div class="sensor__chart">
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__scale" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="256.252" stroke-dashoffset="45.340"></circle>
							                </svg>
							                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__data" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
								                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="221.252" stroke-dashoffset="80.340"></circle>
							                </svg>
							                <div class="sensor__arrow" style="transform: rotate(180deg)"></div>
						                </div>
						                <div class="sensor__value">40&deg;C</div>
					                </div>
					                <div class="data__sensor_legend">потребляемая мощность</div>
				                </div>
			                </div>

		                </div>
	                </div>

                </div>

	            <aside class="main__control">
		            <div class="main__control_toggle"></div>
		            <div class="main__control_wrap" id="control">

			            <div class="control">

				            <div class="control__heading">Функциональные группы 103 (DemoRoom)</div>
				            <div class="control__search">
					            <form class="form">
						            <input class="control__search_input" type="text" placeholder="">
						            <button type="submit" class="control__search_button">
							            <img src="img/icon__search_dark.svg" class="img-fluid" alt="">
						            </button>
					            </form>
				            </div>

				            <table class="control__task">
					            <tr>
						            <td>Задача 4</td>
						            <td><span class="control__task_status yellow">1</span></td>
					            </tr>
					            <tr>
						            <td>Задача 1</td>
						            <td><span class="control__task_status">1</span></td>
					            </tr>
					            <tr>
						            <td>Задача 2</td>
						            <td><span class="control__task_status">3</span></td>
					            </tr>
					            <tr>
						            <td>Задача 3</td>
						            <td><span class="control__task_status">4</span></td>
					            </tr>
				            </table>

				            <div class="control__divider mb_20"></div>

				            <div class="control__tabs">
					            <ul class="control__tabs_nav mb_10">
						            <li class="active"><a href="#tab1">Комментарии</a></li>
						            <li><a href="#tab2">События</a></li>
					            </ul>

					            <div class="control__tabs_item active" id="tab1">
						            <table class="table_column">
							            <tr>
								            <th>Дата</th>
								            <th>Событие</th>
								            <th>Статус</th>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td>Критическая температура</td>
								            <td><span class="color-red text-uppercase">critical</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td>Отключение стойки</td>
								            <td><span class="color-red text-uppercase">critical</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td>Сбой блока питания</td>
								            <td><span class="color-yellow text-uppercase">warning</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
						            </table>
					            </div>
					            <div class="control__tabs_item" id="tab2">
						            <table class="table_column">
							            <tr>
								            <th>Дата</th>
								            <th>Событие</th>
								            <th>Статус</th>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td>Критическая температура</td>
								            <td><span class="color-red text-uppercase">critical</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td>Отключение стойки</td>
								            <td><span class="color-red text-uppercase">critical</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td>Сбой блока питания</td>
								            <td><span class="color-yellow text-uppercase">warning</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
							            <tr>
								            <td><span class="date">04.02.2018</span> <span class="time">12:58:31</span></td>
								            <td></td>
								            <td><span class="text-uppercase">normal</span></td>
							            </tr>
						            </table>
					            </div>

				            </div>

				            <div class="control__divider pt_10"></div>

			            </div>

		            </div>
	            </aside>

                
            </section>
            
        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script src="js/control.js"></script>

    </body>
</html>
