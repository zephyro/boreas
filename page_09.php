<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <section class="main">
    
                <aside class="main__sidebar">

	                <!-- Sidebar -->
                    <?php include('inc/sidebar.inc.php') ?>
	                <!-- -->

                </aside>
    
                <div class="main__content">

	                <div class="hardware">
		                <ul class="hardware__nav">
			                <li class="active"><a href="#tab1"><span>Серверы (10) </span><i></i></a></li>
			                <li><a href="#tab2"><span>Коммутаторы  (2)</span><i></i></a></li>
			                <li><a href="#tab3"><span>СХД  (8)</span><i></i></a></li>
			                <li><a href="#tab4"><span>JBOD (1)</span><i></i></a></li>
			                <li><a href="#tab5"><span>ИБП (1)</span><i></i></a></li>
		                </ul>
		                <div class="hardware__content">

			                <!-- Tab 1 -->
			                <div class="hardware__tab active" id="tab1">

				                <div class="hardware__scroll">

					                <div class="hardware__row">

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							           </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

					                </div>

				                </div>

			                </div>

			                <!-- Tab 2 -->
			                <div class="hardware__tab" id="tab2">
				                <div class="hardware__scroll">

					                <div class="hardware__row">

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm 3429J3</div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm 3429J3</div>
							           </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm 3429J3</div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm 3429J3</div>
							                <div class="hardware__item_info">HOST: alexander12-34.depo.infra.net</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm 3429J3</div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

					                </div>

				                </div>
			                </div>

			                <!-- Tab 3 -->
			                <div class="hardware__tab" id="tab3">

				                <div class="hardware__scroll">

					                <div class="hardware__row">

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

					                </div>

				                </div>

			                </div>

			                <!-- Tab 4 -->
			                <div class="hardware__tab" id="tab4">

				                <div class="hardware__scroll">

					                <div class="hardware__row">

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

					                </div>

				                </div>

			                </div>

			                <!-- Tab 5 -->
			                <div class="hardware__tab" id="tab5">

				                <div class="hardware__scroll">

					                <div class="hardware__row">

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

						                <a href="#" class="hardware__item">
							                <div class="hardware__item_name">Сервер DEPO Storm <span class="color-blue">Alexander</span></div>
							                <div class="hardware__item_image">
								                <img src="images/server_01.jpg" class="img-fluid" alt="">
							                </div>
							                <div class="hardware__item_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <span class="color-blue">alexander</span> ut labore et dolore magna aliqua.</div>
						                </a>

					                </div>

				                </div>

			                </div>

		                </div>

	                </div>



	                <div class="server_group">

		                <div class="server server_red">

			                <div class="server__status server__status_red">1</div>

			                <div class="server__item">

				                <div class="server__unit">
					                <img src="img/server__top.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit server__unit_alert">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_05.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit server__unit_alert">
					                <img src="img/server__block_04.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit server__unit_alert">
					                <img src="img/server__block_03.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__bottom">
					                <img src="img/server__bottom.jpg" class="img-fluid" alt="">
				                </div>
			                </div>

			                <div class="server__name">1 (DemoRoom)</div>

		                </div>

		                <div class="server">

			                <div class="server__status"></div>

			                <div class="server__item">

				                <div class="server__unit">
					                <img src="img/server__top.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_05.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_04.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_03.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__bottom">
					                <img src="img/server__bottom.jpg" class="img-fluid" alt="">
				                </div>
			                </div>

			                <div class="server__name">3 (DemoRoom)</div>

		                </div>

		                <div class="server">

			                <div class="server__status"></div>

			                <div class="server__item">

				                <div class="server__unit">
					                <img src="img/server__top.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_04.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_06.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_03.jpg" class="img-fluid" alt="">
				                </div>

				                <div class="server__unit">
					                <img src="img/server__block_05.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_04.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__divder_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_03.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_02.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__unit">
					                <img src="img/server__block_01.jpg" class="img-fluid" alt="">
				                </div>
				                <div class="server__bottom">
					                <img src="img/server__bottom.jpg" class="img-fluid" alt="">
				                </div>
			                </div>

			                <div class="server__name">3 (DemoRoom)</div>

		                </div>

	                </div>
                    
                </div>
                
            </section>
            
        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script>
            (function() {

                var hScroll = window.Scrollbar;
                hScroll.init(document.querySelector('.active .hardware__scroll'));

                $('.hardware__nav > li > a').on('click touchstart', function(e){
                    e.preventDefault();

                    var tab = $($(this).attr("href"));
                    var box = $(this).closest('.hardware');

                    $(this).closest('.hardware__nav').find('li').removeClass('active');
                    $(this).closest('li').addClass('active');

                    hScroll.destroy(document.querySelector('.active .hardware__scroll'));

                    box.find('.hardware__tab').removeClass('active');
                    box.find(tab).addClass('active');
                    hScroll.init(document.querySelector('.active .hardware__scroll'));
                });

            }());
        </script>

    </body>
</html>
