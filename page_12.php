<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <section class="main">
    
                <aside class="main__sidebar">

	                <!-- Sidebar -->
                    <?php include('inc/sidebar.inc.php') ?>
	                <!-- -->

                </aside>
    
                <div class="main__content">

	                <ul class="breadcrumb">
		                <li><a href="#">Главная</a></li>
		                <li><a href="#">Boreas Lab Rack</a></li>
		                <li>Стойка</li>
	                </ul>

	                <div class="main__heading">
		                <h1>DEPO Storm 3400Z2 </h1>
	                </div>

	                <div class="device_wrap">
		                <div class="device open">
			                <div class="device__heading">
				                <div class="device__status device__status_warning">1</div>
				                <div class="device__status">1</div>
				                <span>БЛОКИ ПИТАНИЯ [2]</span>
			                </div>

			                <div class="device__group">

				                <div class="device__elem">
					                <div class="device__elem_title">Блоки питания [2]</div>
					                <div class="device__elem_body">
						                <div class="device__elem_err">
							                <img src="img/icon__err.svg" class="img-fluid" alt="">
						                </div>
					                </div>
				                </div>

			                </div>
		                </div>
	                </div>

	                <div class="device_wrap">
		                <div class="device open">
			                <div class="device__heading">
				                <div class="device__status  device__status_alert">1</div>
				                <div class="device__status  device__status_warning">1</div>
				                <div class="device__status">1</div>
				                <span>ПРОЦЕССОРЫ [2]</span>
			                </div>
			                <div class="device__group">
				                <div class="device__elem">
					                <div class="device__elem_title">ПРОЦЕССОР (2)</div>
					                <div class="device__elem_body">
						                <div class="sensor sensor_yellow">
							                <div class="sensor__chart">
								                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__scale" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
									                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="256.252" stroke-dashoffset="45.340"></circle>
								                </svg>
								                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__data" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
									                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="221.252" stroke-dashoffset="80.340"></circle>
								                </svg>
								                <div class="sensor__arrow" style="transform: rotate(180deg)"></div>
							                </div>
							                <div class="sensor__value">40&deg;C</div>
						                </div>
					                </div>
				                </div>
				                <div class="device__elem">
					                <div class="device__elem_title">ПРОЦЕССОР (3)</div>
					                <div class="device__elem_body">
						                <div class="sensor sensor_red">
							                <div class="sensor__chart">
								                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__scale" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
									                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="256.252" stroke-dashoffset="45.340"></circle>
								                </svg>
								                <svg style="display: inline-block; vertical-align: middle" id="svg" class="sensor__data" viewport="0 0 96 96" version="1.1" xmlns="http://www.w3.org/2000/svg">
									                <circle id="bar" r="44" cx="48" cy="48" fill="transparent" stroke-dasharray="221.252" stroke-dashoffset="80.340"></circle>
								                </svg>
								                <div class="sensor__arrow" style="transform: rotate(180deg)"></div>
							                </div>
							                <div class="sensor__value">40&deg;C</div>
						                </div>
					                </div>
				                </div>
			                </div>
		                </div>
	                </div>

	                <div class="chart">
		                <div class="chart__title">
			                <div class="chart__title_wrap">
				                <span>График параметров</span>
				                <a class="chart__title_params" href="#">
					                <img src="img/icon_setting.svg" class="img-fluid" alt="">
				                </a>
			                </div>
		                </div>
		                <div class="chart__content">
			                <img src="img/chart.png" class="img-fluid" alt="">
		                </div>
	                </div>

                </div>

	            <aside class="main__control">
		            <div class="main__control_toggle"></div>
		            <div class="main__control_wrap" id="control">
			            <div class="control">

				            <div class="control__heading">Сервер DEPO Storm 3400Z2 </div>

				            <div class="control__rack">
					            <ul class="control__rack_image">
						            <li>
							            <img src="img/unit_02.jpg" class="img-fluid" alt="">
						            </li>
						            <li>
							            <img src="img/unit_01.jpg" class="img-fluid" alt="">
						            </li>
					            </ul>

					            <a class="control__rack_nav control__rack_prev" href="#">
						            <img src="img/control__up.svg" class="img-fluid" alt="">
					            </a>
					            <a class="control__rack_nav control__rack_next" href="#">
						            <img src="img/control__down.svg" class="img-fluid" alt="">
					            </a>
				            </div>

				            <div class="control__row">

					            <div class="control__row_right">

						            <ul class="control__buttons">
							            <li>
								            <a href="#">
									            <img src="img/icon__setting.svg" class="img-fluid" alt="">
								            </a>
							            </li>
							            <li>
								            <a href="#">
									            <img src="img/icon__light.svg" class="img-fluid">
								            </a>
							            </li>
							            <li>
								            <a href="#">
									            <img src="img/icon__message.svg" class="img-fluid" alt="">
								            </a>
							            </li>
							            <li>
								            <a href="#">
									            <img src="img/icon__off.svg" class="img-fluid">
								            </a>
							            </li>
						            </ul>

					            </div>

					            <div class="control__row_left">

						            <ul class="control__info">
							            <li><span>Стойка:</span> 103 (DemoRoom), Позиция: 32</li>
							            <li><span>Юнитов U:</span> 1</li>
							            <li><span>IP:</span> 10.211.2.19</li>
							            <li><span>Серийный номер:</span> 390656-001</li>
							            <li><span>Версия BMC:</span> 3.20</li>
							            <li><span>Версия BIOS:</span> Depov026</li>
							            <li><span>Протокол подключения: IPMI</span></li>
						            </ul>

						            <div class="control__text">Сервер с расширенной дисковой подсистемой построен на отечественной платформе ДЕПО.</div>

					            </div>

				            </div>

				            <div class="control__indicators open">
					            <div class="control__indicators_title">Дополнительные показатели </div>
					            <div class="control__indicators_body">
						            <ul class="control__indicators_table">
							            <li>
								            <strong>ВЕРСИЯ BIOS</strong>
								            <span>4.6.2</span>
							            </li>
							            <li>
								            <strong>ID ХОСТА</strong>
								            <span>248A072AC96A</span>
							            </li>
							            <li>
								            <strong>СЕРИЙНЫЙ НОМЕР</strong>
								            <span>\"MT1634X10305\"</span>
							            </li>
							            <li>
								            <strong>СБОРКА ПРОШИВКИ</strong>
								            <span>18.01.18 17:25</span>
							            </li>
						            </ul>
					            </div>
				            </div>

				            <div class="control__indicators open">
					            <div class="control__indicators_title">Группы и теги</div>
					            <div class="control__indicators_body">
						            <div class="control__group">
							            <ul class="control__tags">
								            <li>
									            <a href="#">
										            <span>Сервера Филиала Орел </span><i class="control__tags_remove"></i>
									            </a>
								            </li>
							            </ul>
							            <span class="control__group_plus">
													<img src="img/control__add.svg" class="img-fluid" alt="">
												</span>
						            </div>
						            <div class="control__group">
							            <div class="control__group_text">Нет функциональных групп</div>
							            <span class="control__group_plus">
													<img src="img/control__add.svg" class="img-fluid" alt="">
												</span>
						            </div>
						            <div class="control__group">
							            <div class="control__group_text">Нет тегов</div>
							            <span class="control__group_plus">
													<img src="img/control__add.svg" class="img-fluid" alt="">
												</span>
						            </div>
					            </div>
				            </div>

				            <div class="control__divider"></div>

				            <div class="control__tabs">
					            <ul class="control__tabs_nav">
						            <li><a href="#tab1">Журнал</a></li>
						            <li><a href="#tab2">Конфигурация <span class="hide-xs-only">модели</span></a></li>
						            <li class="active"><a href="#tab3">Мониторинг</a></li>
					            </ul>

					            <div class="control__tabs_item" id="tab1"></div>
					            <div class="control__tabs_item" id="tab2"></div>
					            <div class="control__tabs_item active" id="tab3">
						            <div class="form_group">
							            <div class="form_inline">
								            <div class="form_label">Параметр мониторинга</div>
								            <div class="form_input">
									            <input type="text" class="form_control" placeholder="Выберете параметр" name="">
								            </div>
							            </div>
						            </div>
						            <div class="form_group">
							            <div class="form_inline">
								            <div class="form_label">Дата</div>
								            <div class="form_input">
									            <div class="form_interval">
										            <div class="form_interval__group">
											            <div class="form_interval__label">от</div>
											            <div class="form_interval__input">
												            <input type="text" class="form_control" placeholder="03.06.18 14:30" name="">
											            </div>
										            </div>
										            <div class="form_interval__group">
											            <div class="form_interval__label">до</div>
											            <div class="form_interval__input">
												            <input type="text" class="form_control" placeholder="03.06.18 14:30" name="">
											            </div>
										            </div>
									            </div>
								            </div>
							            </div>
						            </div>
						            <ul class="btn_group align_right">
							            <li><button class="btn btn_transparent" type="button">Сбросить</button></li>
							            <li><button class="btn" type="submit">Построить график </button></li>
						            </ul>
					            </div>

				            </div>

			            </div>
		            </div>
	            </aside>
                
            </section>
            
        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script src="js/control.js"></script>


    </body>
</html>
