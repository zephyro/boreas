<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <section class="main">
    
                <aside class="main__sidebar">

	                <!-- Sidebar -->
                    <?php include('inc/sidebar.inc.php') ?>
	                <!-- -->

                </aside>
    
                <div class="main__content">

	                <div class="system_nav">
		                <ul>
			                <li><a href="#">Устройства</a></li>
			                <li><a href="#">Группы устройств</a></li>
			                <li><a href="#" class="active">Функциональные группы</a></li>
			                <li><a href="#">Теги</a></li>
			                <li><a href="#">Модели устройств</a></li>
			                <li><a href="#">Каталог моделей</a></li>
			                <li><a href="#">Диапазоны</a></li>
			                <li><a href="#">Пользователи</a></li>
			                <li><a href="#">Уведомления</a></li>
			                <li><a href="#">Параметры мониторинга</a></li>
			                <li><a href="#">Системные</a></li>
		                </ul>
	                </div>

	                <div class="rack">

		                <div class="rack__left">

			                <div class="rack__new">
				                <a href="#" class="btn">Создать стойку</a>
			                </div>

			                <div class="control__search mb_25">
				                <form class="form">
					                <input class="control__search_input control__search_input__double" type="text" placeholder="">
					                <button type="submit" class="control__search_button">
						                <img src="img/icon__search_dark.svg" class="img-fluid" alt="">
					                </button>
				                </form>
			                </div>

			                <div class="rack__info">
				                <table class="rack__table">
					                <tr>
						                <th></th>
						                <th>№</th>
						                <th>Наименование</th>
						                <th>Количество устройств</th>
					                </tr>
					                <tr>
						                <td>
							                <label class="form_checkbox form_checkbox_single">
								                <input type="checkbox" name="" value="" checked>
								                <span></span>
							                </label>
						                </td>
						                <td>234</td>
						                <td>Стойка раз</td>
						                <td>3</td>
					                </tr>
					                <tr>
						                <td>
							                <label class="form_checkbox">
								                <input type="checkbox" name="" value="">
								                <span></span>
							                </label>
						                </td>
						                <td>245</td>
						                <td>Стойка раз</td>
						                <td>0</td>
					                </tr>
				                </table>
			                </div>

		                </div>

		                <div class="rack__right">

			                <div class="rack__title">Редактирование стойки</div>
			                <div class="rack__row">
				                <div class="rack__row_left">
									<div class="rack__server">
										<div class="server__item">

											<div class="server__unit">
												<img src="img/server__top.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_04.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_06.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_03.jpg" class="img-fluid" alt="">
											</div>

											<div class="server__unit">
												<img src="img/server__block_05.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_02.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__block_04.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__divder_01.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__block_02.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__block_03.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__block_02.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__unit">
												<img src="img/server__block_01.jpg" class="img-fluid" alt="">
											</div>
											<div class="server__bottom">
												<img src="img/server__bottom.jpg" class="img-fluid" alt="">
											</div>
										</div>
									</div>

				                </div>
				                <div class="rack__row_right">

					                <div class="form_group">
						                <div class="rack__form_inline">
							                <div class="form_label">Название:</div>
							                <div class="form_input">
								                <input type="text" class="form_control" name="" value="Стойка раз">
							                </div>
						                </div>
					                </div>

					                <div class="form_group">
						                <div class="rack__form_inline">
							                <div class="form_label">Расположение:</div>
							                <div class="form_input">
								                <input type="text" class="form_control" name="" value="Стойка 1">
							                </div>
						                </div>
					                </div>

					                <div class="form_group">
						                <div class="rack__form_inline">
							                <div class="form_label">Высота:</div>
							                <div class="form_input">
								                <input type="text" class="form_control" name="" value="42">
							                </div>
						                </div>
					                </div>

					                <div class="form_group">
						                <div class="rack__form_inline">
							                <div class="form_label">Список устройств:</div>
							                <div class="form_input">
								                <div class="rack__info">
									                <table class="rack__table">
										                <tr>
											                <th></th>
											                <th>№</th>
											                <th>Наименование</th>
											                <th>Количество устройств</th>
										                </tr>
										                <tr>
											                <td>
												                <label class="form_checkbox form_checkbox_single">
													                <input type="checkbox" name="" value="" checked>
													                <span></span>
												                </label>
											                </td>
											                <td>234</td>
											                <td>Стойка раз</td>
											                <td>3</td>
										                </tr>
										                <tr>
											                <td>
												                <label class="form_checkbox">
													                <input type="checkbox" name="" value="">
													                <span></span>
												                </label>
											                </td>
											                <td>245</td>
											                <td>Стойка раз</td>
											                <td>0</td>
										                </tr>
									                </table>
								                </div>
							                </div>
						                </div>
					                </div>

					                <ul class="btn_group flex_right">
						                <li>
							                <button type="button" class="btn btn_transparent">Отмена</button>
						                </li>
						                <li>
							                <button type="button" class="btn">Обновить</button>
						                </li>
					                </ul>

				                </div>
			                </div>

		                </div>

	                </div>
                    
                </div>
                
            </section>
            
        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script>
            (function() {

                var hScroll = window.Scrollbar;
                hScroll.init(document.querySelector('.active .hardware__scroll'));

                $('.hardware__nav > li > a').on('click touchstart', function(e){
                    e.preventDefault();

                    var tab = $($(this).attr("href"));
                    var box = $(this).closest('.hardware');

                    $(this).closest('.hardware__nav').find('li').removeClass('active');
                    $(this).closest('li').addClass('active');

                    hScroll.destroy(document.querySelector('.active .hardware__scroll'));

                    box.find('.hardware__tab').removeClass('active');
                    box.find(tab).addClass('active');
                    hScroll.init(document.querySelector('.active .hardware__scroll'));
                });

            }());
        </script>

    </body>
</html>
